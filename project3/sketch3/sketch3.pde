//import statements
import java.util.Scanner;
//global declarations
ArrayList<String> lines = new ArrayList<String>();
float x,y,xspacer,yspacer;
//dimensions
int margin = 70;
int innermargin = 10;
//set up method will be running first
void setup(){
  size(800,600);
  background(0);
  selectInput("Select a data file to process:", "callbackfunction");
}
/***********************************************************************/
/*call back function after selecting a file or cloasing the window */
/***********************************************************************/
public void callbackfunction(File s) throws Exception{
  if(s == null){
    println("file not selected");
  }
  else{
        /*file is selected so loading the string into the list */
        println("you have selected " + s.getAbsolutePath());  
        Scanner sc = new Scanner(s); 
        while(sc.hasNext()){
          lines.add(sc.next());
        }
        sc.close();  
  }
}

/*************************************************************************/
/* darw method */
/*************************************************************************/
void draw(){
  /* delay to start delay after loading the list and avoid null pointer exception */
  delay(3000);  
  int i =1;
  while(i<lines.size()){    
    String[] pieces = lines.get(i).split(",");
    /* initializing the width and height of each box */
    xspacer = (width-2*margin-innermargin*3)/4; 
    yspacer = (height-2*margin-3*innermargin)/4;    
    /*colouring the eclipses */
    fill(255,255,0);
    /* mapping the attributes to the dimensions using the range and plotting them in respective boxes in the matrix*/
    x = map(float(pieces[0]),0,max(lines,0),margin, margin+xspacer);
    y = map(float(pieces[1]),0,max(lines,1),margin+yspacer,margin);   
    ellipse(x,y+innermargin+yspacer,2,2);
    
    x = map(float(pieces[1]),0,max(lines,1),margin, margin+xspacer);
    y = map(float(pieces[0]),0,max(lines,0),margin+yspacer,margin);
    ellipse(x+innermargin+xspacer,y,2,2);
 
    x = map(float(pieces[2]),0,max(lines,2),margin, margin+xspacer);
    y = map(float(pieces[0]),0,max(lines,0),margin+yspacer,margin);
    ellipse(x+2*innermargin+2*xspacer,y,2,2);
   
    x = map(float(pieces[3]),0,max(lines,3),margin, margin+xspacer);
    y = map(float(pieces[0]),0,max(lines,0),margin+yspacer,margin);
    ellipse(x+3*innermargin+3*xspacer,y,2,2);
   
    x = map(float(pieces[2]),0,max(lines,2),margin, margin+xspacer);
    y = map(float(pieces[1]),0,max(lines,1),margin+yspacer,margin);   
    ellipse(x+2*innermargin+2*xspacer,y+innermargin+yspacer,2,2);
   
    x = map(float(pieces[3]),0,max(lines,3),margin, margin+xspacer);
    y = map(float(pieces[1]),0,max(lines,1),margin+yspacer,margin);   
    ellipse(x+3*innermargin+3*xspacer,y+innermargin+yspacer,2,2);
   
    x = map(float(pieces[0]),0,max(lines,0),margin, margin+xspacer);
    y = map(float(pieces[2]),0,max(lines,2),margin+yspacer,margin);   
    ellipse(x,y+2*innermargin+2*yspacer,2,2);
      
    x = map(float(pieces[1]),0,max(lines,1),margin, margin+xspacer);
    y = map(float(pieces[2]),0,max(lines,2),margin+yspacer,margin);   
    ellipse(x+innermargin+xspacer,y+2*innermargin+2*yspacer,2,2);
   
    x = map(float(pieces[3]),0,max(lines,3),margin, margin+xspacer);
    y = map(float(pieces[2]),0,max(lines,2),margin+yspacer,margin);   
    ellipse(x+3*innermargin+3*xspacer,y+2*innermargin+2*yspacer,2,2);
   
    x = map(float(pieces[0]),0,max(lines,0),margin, margin+xspacer);
    y = map(float(pieces[3]),0,max(lines,3),margin+yspacer,margin);   
    ellipse(x,y+3*innermargin+3*yspacer,2,2);
   
    x = map(float(pieces[1]),0,max(lines,1),margin, margin+xspacer);
    y = map(float(pieces[3]),0,max(lines,3),margin+yspacer,margin);   
    ellipse(x+innermargin+xspacer,y+3*innermargin+3*yspacer,2,2);
   
    x = map(float(pieces[2]),0,max(lines,2),margin, margin+xspacer);
    y = map(float(pieces[3]),0,max(lines,3),margin+yspacer,margin);   
    ellipse(x+2*innermargin+2*xspacer,y+3*innermargin+3*yspacer,2,2);
   
    x = map(float(pieces[2]),0,max(lines,2),margin, margin+xspacer);
    y = map(float(pieces[3]),0,max(lines,3),margin+yspacer,margin);   
    ellipse(x+2*innermargin+2*xspacer,y+3*innermargin+3*yspacer,2,2);     
    i++;
    }
    /* calling function to insert matrix and labels */
    if(i==lines.size()){
     labelsandmatrix();
    }  
    /* making the draw loop execute only once */
    noLoop(); 
}