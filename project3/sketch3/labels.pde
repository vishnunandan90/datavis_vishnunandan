public void labelsandmatrix(){
  stroke(100,100,100);
  /* labels*/
    for(int g=0;g<4;g++){    
    String[] temp = lines.get(0).split(",");
    text(temp[g],margin+xspacer/2+g*(xspacer+innermargin),height-margin/4);
    text(temp[g],margin/10,margin+yspacer/2+g*(innermargin+yspacer));
  }
    
    /*matrix */
     line(margin-innermargin,margin+yspacer+innermargin/2,width-margin+innermargin,margin+yspacer+innermargin/2);
     line(margin-innermargin,margin+2*yspacer+3*innermargin/2,width-margin+innermargin,margin+2*yspacer+3*innermargin/2);
     line(margin-innermargin,margin+3*yspacer+5*innermargin/2,width-margin+innermargin,margin+3*yspacer+5*innermargin/2);
     line(margin+xspacer+innermargin/2,margin-innermargin,margin+xspacer+innermargin/2,height-margin+innermargin);
     line(margin+2*xspacer+3*innermargin/2,margin-innermargin,margin+2*xspacer+3*innermargin/2,height-margin+innermargin);
     line(margin+3*xspacer+5*innermargin/2,margin-innermargin,margin+3*xspacer+5*innermargin/2,height-margin+innermargin);
     line(margin-innermargin,margin-innermargin,margin-innermargin,height-margin+innermargin);
     line(margin-innermargin,margin-innermargin,width-margin+innermargin,margin-innermargin);
     line(width-margin+innermargin,margin-innermargin,width-margin+innermargin,height-margin+innermargin);
     line(margin-innermargin,height-margin+innermargin,width-margin+innermargin,height-margin+innermargin);
    /* axis tickmarks and values */    
    for(int a=0;a<=3;a++){
     ellipse((margin+xspacer/2)+a*(xspacer+innermargin),height-margin+innermargin,2,2);  
     ellipse(margin+a*(xspacer+innermargin),height-margin+innermargin,2,2);
     ellipse((margin+xspacer)+a*(xspacer+innermargin),height-margin+innermargin,2,2);
     ellipse(margin-innermargin,height-margin-a*(yspacer+innermargin),2,2);
     ellipse(margin-innermargin,height-margin-yspacer/2-a*(yspacer+innermargin),2,2);
     ellipse(margin-innermargin,height-margin-yspacer-a*(yspacer+innermargin),2,2);
     fill(100,100,100);
     text(int(max(lines,a)/2),(margin+xspacer/2)+a*(xspacer+innermargin),height-margin+innermargin+20); 
     text("0",margin+a*(xspacer+innermargin),height-margin+innermargin+20);   
     text(int(max(lines,a)),(margin+xspacer)+a*(xspacer+innermargin)-20,height-margin+innermargin+20);
     text("0",margin-innermargin-10,height-margin-a*(yspacer+innermargin));
     text(int(max(lines,a)/2),margin-innermargin-20,height-margin-yspacer/2-a*(yspacer+innermargin));   
     text(int(max(lines,a)),margin-innermargin-20,height-margin-yspacer-a*(yspacer+innermargin)+10);
}    fill(255,255,0);
     text("scatter plots",(width/2)-20,margin/2);
}