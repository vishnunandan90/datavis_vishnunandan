/*import statements */
import java.util.Scanner;
//global declarations
ArrayList<String> lines = new ArrayList<String>();
/* dimensions for axes */
int margin = 100;
float xmax,ymax,xgapvalue,ygapvalue;
/********************************************************************
*********setup function runs first***********************************
*********************************************************************/
void setup(){
  size(800,600);
  background(0);
  selectInput("Select a data file to process:", "callbackfunction");
}
/*callback function if file selected or window closed */
public void callbackfunction(File s) throws Exception{
  if(s == null){
    println("file not selected");
  }
  else{
    println("you have selected " + s.getAbsolutePath());
    Scanner sc = new Scanner(s);     
    while(sc.hasNext()){
      lines.add(sc.next());
    }
   sc.close();
  }
 
}
void draw(){
 /* delay to avoid null pointer exception */
  delay(3000);
  int i =1;
  while(i<lines.size()){    
    String[] pieces = lines.get(i).split(",");
    float x = map(float(pieces[2]),0,max(lines,2),margin,width-margin);
    if(float(pieces[2])==max(lines,2)){
      xmax=x;
    }    
    float y = map(float(pieces[3]),0,max(lines,3),height-margin,margin);
    if(float(pieces[3])==max(lines,3)){
      ymax=y;
    }
    fill(255,255,0);
    ellipse(x,y,3,3);
    i++;
   }
   /* initializing values for axes */
    xgapvalue = max(lines,2)/10;
    ygapvalue = max(lines,3)/10;
    /* calling function for drawing axes */
    drawaxis(xmax,xgapvalue,ymax,ygapvalue);
    /* avoiding the repeated execution of draw */
    noLoop();
  }