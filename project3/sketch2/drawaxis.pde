/*this function draws axes according to the range of attributes */
public void drawaxis(float xm, float xg, float ym, float yg){
  float xgv = (xm-3*margin/4)/10;
  float ygv = ((height-3*margin/4)-ym)/10;
  line(3*margin/4,height-3*margin/4,width,height-3*margin/4);
  line(3*margin/4,height-3*margin/4,3*margin/4,0);
  fill(100,100,100);
  stroke(100,100,100);
  /* axes */
  line(3*margin/4,height-3*margin/4,width,height-3*margin/4);
  line(3*margin/4,height-3*margin/4,3*margin/4,0);
  for(int k=0;k<=10;k++){
    fill(100,100,100);
    stroke(100,100,100);
    ellipse(3*margin/4,height-3*margin/4-k*ygv,2,2);
    text((k*yg),margin/4,height-3*margin/4-k*ygv);
    ellipse((k*xgv)+3*margin/4,height-3*margin/4,2,2);
    text((k*xg),k*xgv+3*margin/4,height-margin/2);
    fill(255,255,0);
    /* labels */ 
    String[] temp = lines.get(0).split(",");
    text(temp[2],width/2,height-margin/4);
    text(temp[3],margin/7,height/2);
    text("ACT vs GPA", (width/2)-50,margin/2);
  }
}