import java.util.Scanner;
//global declarations
ArrayList<String> lines = new ArrayList<String>();
//dimensions for axes
int margin = 100;
float xmax,ymax;
float xgapvalue,ygapvalue;

/****************************************************************/
/* setup function runs first */
/****************************************************************/
void setup(){
  /*setting size and background */
  size(800,600);
  background(0);
  selectInput("Select a data file to process:", "callbackfunction");
}
/*****************************************************************/
/*call back function when a file is selected or window is closed */
/*****************************************************************/

public void callbackfunction(File s) throws Exception{
  if(s == null){
    println("file not selected");
  }
  else{
    /* loading the file data into the list */
    println("you have selected " + s.getAbsolutePath());        
    Scanner sc = new Scanner(s);     
    while(sc.hasNext()){
     lines.add(sc.next());
    }
    sc.close();
  
  }
 
}
/* draw method */
void draw(){
  /* delay to avoid null pointer exception and draw execution before loading the list */
  delay(8000);  
  int i =1;
  while(i<lines.size()){    
    String[] pieces = lines.get(i).split(",");
    /* mapping the attribtes to corresponding axes and drawing ellipses 
    with radius according to GPA attribute */
    float x = map(float(pieces[0]),0,max(lines,0),margin,width-3*margin/4);
    if(int(pieces[0])==max(lines,0)){
      xmax=x;
    }
    float y = map(float(pieces[1]),0,max(lines,1),height-margin,margin);
    if(int(pieces[1])==max(lines,1)){
      ymax=y;
    }
    fill(255,255,0);
    ellipse(x,y,float(pieces[3])*1.5,float(pieces[3])*1.5);    
    i++;
    }
    /* initializing the variables required for axes and tick marks and calling the function */
    xgapvalue = max(lines,0)/10;
    ygapvalue = max(lines,1)/10;
    if(i==lines.size()){
      drawaxis(xmax,xgapvalue,ymax,ygapvalue);
    }
    /* avoiding repeated execution of draw method */
    noLoop();
  }