//this method will display the corresponding data and highlight the line when mouse is over it.
void dataDisp(float mx,float my){
   int count=0;
   Plot plot5 = new Plot(10,610,1180,height-margin,height-margin-(height-3*margin)/2);
   float gap = ((plot5.endx-plot5.startx)-2*plot5.plotmargin)/3;
   for(int i=0;i<linearray.size();i++){
    if(perpendicularDistance(mx,my,linearray.get(i))<0.5){
      count++;
        if(count==1){
          textSize(13);
          stroke(255,0,0);
          strokeWeight(4);
          float te1,te2,te3,te4;
           te1 = map(linearray.get(i).c1,Collections.min(wc.get(0)),Collections.max(wc.get(0)),plot5.starty-plot5.plotmargin,plot5.endy+plot5.plotmargin);
           te2 = map(linearray.get(i).c2,Collections.min(wc.get(1)),Collections.max(wc.get(1)),plot5.starty-plot5.plotmargin,plot5.endy+plot5.plotmargin);          
           te3 = map(linearray.get(i).c3,Collections.min(wc.get(2)),Collections.max(wc.get(2)),plot5.starty-plot5.plotmargin,plot5.endy+plot5.plotmargin);
           te4 = map(linearray.get(i).c4,Collections.min(wc.get(3)),Collections.max(wc.get(3)),plot5.starty-plot5.plotmargin,plot5.endy+plot5.plotmargin);         
           line(plot5.startx+plot5.plotmargin,te1,plot5.startx+plot5.plotmargin+gap,te2);
           line(plot5.startx+plot5.plotmargin+gap,te2,plot5.startx+plot5.plotmargin+2*gap,te3);
           line(plot5.startx+plot5.plotmargin+2*gap,te3,plot5.startx+plot5.plotmargin+3*gap,te4);
           strokeWeight(1);
           stroke(0);
           fill(100,80,9); 
           //fill(0);
           //stroke(100);
          text(linearray.get(i).c1,100,100);
          text(linearray.get(i).c2,margin+gap,10+height-margin/2);
          text(linearray.get(i).c3,margin+2*gap,10+height-margin/2);
          text(linearray.get(i).c4,margin+3*gap,10+height-margin/2);
            Plot plot1 = new Plot(20,20,20+(1120/3),390,20);
             float barwidth = (plot1.endx-plot1.startx-2*plot1.plotmargin)/lines.size();
           fill(200,0,0);
          Plot plot2 = new Plot(20,40+(1120/3),40+2*(1120/3),390,20);
       Plot plot3 = new Plot(20,3*margin+2*(1120/3),3*margin+3*(1120/3),390,20);
          if(i-lines.size()<0){
           rect(plot1.plotmargin+plot1.startx+(i%lines.size()-1)*barwidth,plot1.starty-plot1.plotmargin-map(temp2.get(i%lines.size()),Collections.min(temp2),Collections.max(temp2),0,330),barwidth,map(temp2.get((i%lines.size())),Collections.min(temp2),Collections.max(temp2),0,330));
         ellipse(plot2.plotmargin+plot2.startx+(i%lines.size())*barwidth-2,plot2.starty-plot2.plotmargin-map(temp2.get(i%lines.size()),Collections.min(temp2),Collections.max(temp2),0,330),6,6);  
     float s1 = map(temp1.get(i%lines.size()),0,Collections.max(temp1),0,(1120/3)-2*plot3.plotmargin);
     float s2 = map(temp2.get(i%lines.size()),0,Collections.max(temp2),330,0);
     fill(200,60,0);
     ellipse(s1+3*margin+2*((width-4*margin)/3),s2+plot3.plotmargin+margin,8,8);
        } else if((0<i-lines.size())&&(i-lines.size()<lines.size())){
          rect(plot1.plotmargin+plot1.startx+(i%lines.size())*barwidth,plot1.starty-plot1.plotmargin-map(temp2.get(i%lines.size()+1),Collections.min(temp2),Collections.max(temp2),0,330),barwidth,map(temp2.get((i%lines.size())+1),Collections.min(temp2),Collections.max(temp2),0,330));
   ellipse(plot2.plotmargin+plot2.startx+(i%lines.size()+1)*barwidth-2,plot2.starty-plot2.plotmargin-map(temp2.get(i%lines.size()+1),Collections.min(temp2),Collections.max(temp2),0,330),6,6);  
   float s1 = map(temp1.get(i%lines.size()+1),0,Collections.max(temp1),0,(1120/3)-2*plot3.plotmargin);
     float s2 = map(temp2.get(i%lines.size()+1),0,Collections.max(temp2),330,0);
     fill(200,60,0);
     ellipse(s1+3*margin+2*((width-4*margin)/3),s2+plot3.plotmargin+margin,8,8);
        } else{
          rect(plot1.plotmargin+plot1.startx+(i%lines.size()+1)*barwidth,plot1.starty-plot1.plotmargin-map(temp2.get(i%lines.size()+2),Collections.min(temp2),Collections.max(temp2),0,330),barwidth,map(temp2.get((i%lines.size())+2),Collections.min(temp2),Collections.max(temp2),0,330));
         ellipse(plot2.plotmargin+plot2.startx+(i%lines.size()+2)*barwidth-2,plot2.starty-plot2.plotmargin-map(temp2.get(i%lines.size()+2),Collections.min(temp2),Collections.max(temp2),0,330),6,6);        
      float s1 = map(temp1.get(i%lines.size()+2),0,Collections.max(temp1),0,(1120/3)-2*plot3.plotmargin);
     float s2 = map(temp2.get(i%lines.size()+2),0,Collections.max(temp2),330,0);
     fill(200,60,0);
     ellipse(s1+3*margin+2*((width-4*margin)/3),s2+plot3.plotmargin+margin,8,8); 
     }
      
   
          // ellipse(plot2.plotmargin+plot2.startx+(i%lines.size())*barwidth-2,plot2.starty-plot2.plotmargin-map(temp2.get(i%lines.size()),Collections.min(temp2),Collections.max(temp2),0,330),6,6);  
  
        // if((0<i-lines.size())&&(i-lines.size()<lines.size()){
        //  rect(plot1.plotmargin+plot1.startx+(i%lines.size())*barwidth,plot1.starty-plot1.plotmargin-map(temp2.get(i%lines.size()+2),Collections.min(temp2),Collections.max(temp2),0,330),barwidth,map(temp2.get((i%lines.size())+2),Collections.min(temp2),Collections.max(temp2),0,330));
        //}
        
        
          
        //  else{
        //  rect(plot1.plotmargin+plot1.startx+(i%lines.size())*barwidth,plot1.starty-plot1.plotmargin-map(temp2.get(i%lines.size()+3),Collections.min(temp2),Collections.max(temp2),0,330),barwidth,map(temp2.get((i%lines.size())+3),Collections.min(temp2),Collections.max(temp2),0,330));
        //  }
          
          
          for(int y=0;i<lines.size();i++){
          if((column0.get(y)==linearray.get(i).c1)&&(column1.get(y)==linearray.get(i).c2)&&(column2.get(y)==linearray.get(i).c3)&&(column3.get(y)==linearray.get(i).c4)){
            fill(200,0,0);
           // rect(plot1.plotmargin+plot1.startx+(y)*barwidth,plot1.starty-plot1.plotmargin-map(temp2.get(y+1),Collections.min(temp2),Collections.max(temp2),0,330),barwidth,map(temp2.get(y+1),Collections.min(temp2),Collections.max(temp2),0,330));
          }
          }
          
          fill(0);
          
        }
    }
  }
}