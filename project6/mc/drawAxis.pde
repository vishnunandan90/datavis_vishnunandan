void drawAxis(){
  Plot plot5 = new Plot(10,610,1180,height-margin,height-margin-(height-3*margin)/2);
  plot5.createPlot();
  float gap = ((plot5.endx-plot5.startx)-2*plot5.plotmargin)/3;
  for(int i=0;i<4;i++){
    stroke(0);
    strokeWeight(2);
   line(plot5.startx+plot5.plotmargin+i*gap,plot5.endy+plot5.plotmargin,plot5.startx+plot5.plotmargin+i*gap,plot5.starty-plot5.plotmargin);
   ellipse(margin+i*gap,margin-10,2,2);
   ellipse(margin+i*gap,height-margin+10,2,2);
  // noStroke();
    strokeWeight(1);
  }
 
}