
void labels(){
  Plot plot1 = new Plot(20,20,20+(1120/3),390,20);
   Plot plot2 = new Plot(20,40+(1120/3),40+2*(1120/3),390,20);
   Plot plot3 = new Plot(20,3*margin+2*(1120/3),3*margin+3*(1120/3),390,20);
   Plot plot5 = new Plot(10,610,1180,height-margin,height-margin-(height-3*margin)/2);
 // rotate(HALF_PI);
 // Plot plot5 = new Plot(10,610,1180,height-margin,height-margin-(height-3*margin)/2);
  float gap = ((plot5.endx-plot5.startx)-2*plot5.plotmargin)/3;
 textSize(15);
 text("DASHBOARD",width/2-10,15);
 textSize(10);
pushMatrix();
fill(200,50,70);
  translate(15,250);
  rotate(1.5*PI);
   translate(-15,-250);
  text(labely,15,250);
//   translate(-x,-y);
  popMatrix(); 


text(labelx,150,height/2);
pushMatrix();
fill(200,50,70);
  translate(410,250);
  rotate(1.5*PI);
   translate(-410,-250);
  text(labely,410,250);
//   translate(-x,-y);
  popMatrix(); 


text(labelx,545,height/2);
pushMatrix();
fill(200,50,70);
  translate(805,250);
  rotate(1.5*PI);
   translate(-805,-250);
  text(labely,805,250);
//   translate(-x,-y);
  popMatrix(); 
text(labelx,940,height/2);
for(int i=0;i<4;i++){
  
  text(titles.get(i),plot5.startx+plot5.plotmargin+i*gap,plot5.endy+plot5.plotmargin);
text(Math.round(Collections.max(wc.get(i))),plot5.startx+plot5.plotmargin+i*gap-20,plot5.endy+plot5.plotmargin);
//plot5.startx+plot5.plotmargin+i*gap,plot5.starty-plot5.plotmargin
text(Math.round(Collections.min(wc.get(i))),plot5.startx+plot5.plotmargin+i*gap,plot5.starty-plot5.plotmargin+7);
text("PARALLEL PLOT",850,790);
pushMatrix();
fill(200,50,70);
  translate(30,490);
  rotate(1.5*PI);
   translate(-30,-490);
text(titles.get(1),30,490);
//   translate(-x,-y);
  popMatrix(); 
  
  pushMatrix();
fill(200,50,70);
  translate(30,580);
  rotate(1.5*PI);
   translate(-30,-580);
text(titles.get(2),30,580);
//   translate(-x,-y);
  popMatrix(); 
 pushMatrix();
fill(200,50,70);
  translate(30,670);
  rotate(1.5*PI);
   translate(-30,-670);
text(titles.get(3),30,670);
//   translate(-x,-y);
  popMatrix(); 

text(titles.get(0),150,height-60);
text(titles.get(1),290,height-60);
text(titles.get(2),450,height-60);


text(Math.round((Collections.min(temp2))),plot1.startx-15,plot1.starty-plot1.plotmargin);
text(Math.round(Collections.max(temp2)),400+plot1.startx-20,plot1.endy+plot1.plotmargin);
text(Math.round(Collections.min(temp2)),400+plot1.startx-20,plot1.starty-plot1.plotmargin);
text(Math.round(Collections.max(temp2)),plot1.startx-15,plot1.endy+plot1.plotmargin);
text(0,800+plot1.startx-20,plot1.starty-plot1.plotmargin);
text(Math.round(Collections.max(temp2)),800+plot1.startx-25,plot1.endy+plot1.plotmargin);
textSize(15);
text("bargraph",plot1.startx+120,plot1.endy+15);
text("linegraph",plot1.startx+520,plot1.endy+15);
text("scatter plot",plot1.startx+900,plot1.endy+15);
text("scatter matrix",plot1.startx+200,plot1.endy+405);


textSize(15);
stroke(0);
text("for changing axis press a,b,c,d,e,f",30,15);
textSize(15);
text("hover the mouse over graphs for interaction between graphs", width-450,10);
textSize(10);

}
}