void clicked(int mx,int my){
//clicked on bargraph
float tem1,tem2,tem3,tem4;

  Plot plot1 = new Plot(20,20,20+(1120/3),390,20);
   Plot plot2 = new Plot(20,40+(1120/3),40+2*(1120/3),390,20);
   Plot plot3 = new Plot(20,3*margin+2*(1120/3),3*margin+3*(1120/3),390,20);
   Plot plot5 = new Plot(10,610,1180,height-margin,height-margin-(height-3*margin)/2);
    float gap = ((plot5.endx-plot5.startx)-2*plot5.plotmargin)/3;
  float barwidth = (plot1.endx-plot1.startx-2*plot1.plotmargin)/lines.size();
  if((mx>plot1.startx)&&(mx<plot1.endx)&&(my<plot1.starty)&&(my>plot1.endy)){
    for(int i=0;i<lines.size()-1;i++){
      int count=0;
    if(((mx>plot1.startx+plot1.plotmargin+i*barwidth)&&(mx<plot1.startx+plot1.plotmargin+(i+1)*barwidth)
    &&(my<plot1.starty-plot1.plotmargin)&&(my>plot1.starty-plot1.plotmargin-map(temp2.get(i),Collections.min(temp2),Collections.max(temp2),0,330)))){
      count++;
      if(count==1){
    tem1 = map(column0.get(i+1),Collections.min(column0),Collections.max(column0),plot5.starty-plot5.plotmargin,plot5.endy+plot5.plotmargin);
           tem2 = map(column1.get(i+1),Collections.min(column1),Collections.max(column1),plot5.starty-plot5.plotmargin,plot5.endy+plot5.plotmargin);          
           tem3 = map(column2.get(i+1),Collections.min(column2),Collections.max(column2),plot5.starty-plot5.plotmargin,plot5.endy+plot5.plotmargin);
           tem4 = map(column3.get(i+1),Collections.min(column3),Collections.max(column3),plot5.starty-plot5.plotmargin,plot5.endy+plot5.plotmargin); 
           textSize(13);
          stroke(255,0,0);
          strokeWeight(4);
            line(plot5.startx+plot5.plotmargin,tem1,plot5.startx+plot5.plotmargin+gap,tem2);
           line(plot5.startx+plot5.plotmargin+gap,tem2,plot5.startx+plot5.plotmargin+2*gap,tem3);
           line(plot5.startx+plot5.plotmargin+2*gap,tem3,plot5.startx+plot5.plotmargin+3*gap,tem4);
            strokeWeight(1);
           stroke(0);
           //fill(100,80,9); 
    fill(200,80,90);
    if(i<lines.size()-2){
     rect(plot1.plotmargin+plot1.startx+(i)*barwidth,plot1.starty-plot1.plotmargin-map(temp2.get(i+1),Collections.min(temp2),Collections.max(temp2),0,330),barwidth,map(temp2.get(i+1),Collections.min(temp2),Collections.max(temp2),0,330));
    ellipse(plot2.plotmargin+plot2.startx+(i)*barwidth,plot2.starty-plot2.plotmargin-map(temp2.get(i+1),Collections.min(temp2),Collections.max(temp2),0,330),6,6);  
   
    stroke(0);
     fill(100,80,9); 
     float s1 = map(temp1.get(i+1),0,Collections.max(temp1),0,(1120/3)-2*plot3.plotmargin);
     float s2 = map(temp2.get(i+1),0,Collections.max(temp2),330,0);
     fill(200,60,0);
     ellipse(s1+3*margin+2*((width-4*margin)/3),s2+plot3.plotmargin+margin,8,8);
     fill(0);
    }
     
}
}
}
  }
for (int i=0;i<lines.size()-1;i++){
  int count=0;
  
  float s3 = map(temp2.get(i),Collections.min(temp2),Collections.max(temp2),0,330);
  if((mx<plot2.startx+plot2.plotmargin+(i)*barwidth+2)&&(mx>plot2.startx+plot2.plotmargin+(i)*barwidth-4)&&(my>plot2.starty-plot2.plotmargin-s3-2)&&(my<plot2.starty-plot2.plotmargin-s3+2)){
    count++;
    if(count==1){
    textSize(13);
          stroke(255,0,0);
          strokeWeight(4);
           tem1 = map(column0.get(i),Collections.min(column0),Collections.max(column0),plot5.starty-plot5.plotmargin,plot5.endy+plot5.plotmargin);
           tem2 = map(column1.get(i),Collections.min(column1),Collections.max(column1),plot5.starty-plot5.plotmargin,plot5.endy+plot5.plotmargin);          
           tem3 = map(column2.get(i),Collections.min(column2),Collections.max(column2),plot5.starty-plot5.plotmargin,plot5.endy+plot5.plotmargin);
           tem4 = map(column3.get(i),Collections.min(column3),Collections.max(column3),plot5.starty-plot5.plotmargin,plot5.endy+plot5.plotmargin); 
            line(plot5.startx+plot5.plotmargin,tem1,plot5.startx+plot5.plotmargin+gap,tem2);
           line(plot5.startx+plot5.plotmargin+gap,tem2,plot5.startx+plot5.plotmargin+2*gap,tem3);
           line(plot5.startx+plot5.plotmargin+2*gap,tem3,plot5.startx+plot5.plotmargin+3*gap,tem4);
            strokeWeight(1);
           stroke(0);
           //fill(100,80,9); 
    fill(200,80,90);
    //fill(200,80,90);
    if(i<lines.size()-2){
     rect(plot1.plotmargin+plot1.startx+(i-1)*barwidth,plot1.starty-plot1.plotmargin-map(temp2.get(i),Collections.min(temp2),Collections.max(temp2),0,330),barwidth,map(temp2.get(i),Collections.min(temp2),Collections.max(temp2),0,330));
    ellipse(plot2.plotmargin+plot2.startx+(i)*barwidth-2,plot2.starty-plot2.plotmargin-map(temp2.get(i),Collections.min(temp2),Collections.max(temp2),0,330),6,6);  
    }
   text("pressed",2,100);
   stroke(0);
     fill(100,80,9); 
    float s1 = map(temp1.get(i),0,Collections.max(temp1),0,(1120/3)-2*plot3.plotmargin);
     float s2 = map(temp2.get(i),0,Collections.max(temp2),330,0);
     fill(200,60,0);
     ellipse(s1+3*margin+2*((width-4*margin)/3),s2+plot3.plotmargin+margin,8,8);
     fill(0);  
    }
  }
}
  

for(int i=0;i<lines.size()-1;i++){
  int count=0;
   float x = map(temp1.get(i), 0,Collections.max(temp1),0,(1120/3)-2*plot3.plotmargin);
  float y = map(temp2.get(i),0,Collections.max(temp2),330,0);
  if((mx<x+3*margin+2*((width-4*margin)/3)+3)&&(mx>x+3*margin+2*((width-4*margin)/3)-3)&&(my<y+plot3.plotmargin+margin+3)&&(my>y+plot3.plotmargin+margin-3)){
    count++;
    if(count==1){
    textSize(13);
          stroke(255,0,0);
          strokeWeight(4);
           tem1 = map(column0.get(i),Collections.min(column0),Collections.max(column0),plot5.starty-plot5.plotmargin,plot5.endy+plot5.plotmargin);
           tem2 = map(column1.get(i),Collections.min(column1),Collections.max(column1),plot5.starty-plot5.plotmargin,plot5.endy+plot5.plotmargin);          
           tem3 = map(column2.get(i),Collections.min(column2),Collections.max(column2),plot5.starty-plot5.plotmargin,plot5.endy+plot5.plotmargin);
           tem4 = map(column3.get(i),Collections.min(column3),Collections.max(column3),plot5.starty-plot5.plotmargin,plot5.endy+plot5.plotmargin); 
            line(plot5.startx+plot5.plotmargin,tem1,plot5.startx+plot5.plotmargin+gap,tem2);
           line(plot5.startx+plot5.plotmargin+gap,tem2,plot5.startx+plot5.plotmargin+2*gap,tem3);
           line(plot5.startx+plot5.plotmargin+2*gap,tem3,plot5.startx+plot5.plotmargin+3*gap,tem4);
            strokeWeight(1);
           stroke(0);
    
    fill(200,80,90);
    if(i<lines.size()-2){
     rect(plot1.plotmargin+plot1.startx+(i-1)*barwidth,plot1.starty-plot1.plotmargin-map(temp2.get(i),Collections.min(temp2),Collections.max(temp2),0,330),barwidth,map(temp2.get(i),Collections.min(temp2),Collections.max(temp2),0,330));
    ellipse(plot2.plotmargin+plot2.startx+(i)*barwidth-2,plot2.starty-plot2.plotmargin-map(temp2.get(i),Collections.min(temp2),Collections.max(temp2),0,330),6,6);  }
   text("pressed",2,100);
   stroke(0);
     fill(100,80,9); 
     float s1 = map(temp1.get(i),0,Collections.max(temp1),0,(1120/3)-2*plot3.plotmargin);
     float s2 = map(temp2.get(i),0,Collections.max(temp2),330,0);
     fill(200,60,0);
     ellipse(s1+3*margin+2*((width-4*margin)/3),s2+plot3.plotmargin+margin,8,8);
     fill(0);  
    }
  }
}

}