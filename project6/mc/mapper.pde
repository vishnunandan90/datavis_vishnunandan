//this method will draw the lines according to the axisinvert values 
void mapper(int k,int i){
  float t1,t2;
   Plot plot5 = new Plot(10,610,1180,height-margin,height-margin-(height-3*margin)/2);
   float gap = (plot5.endx-plot5.startx-2*plot5.plotmargin)/3;
   if(k==0){
   t1 = map(column0.get(i),Collections.min(column0),Collections.max(column0),plot5.starty-plot5.plotmargin,plot5.endy+plot5.plotmargin);
   t2 = map(column1.get(i),Collections.min(column1),Collections.max(column1),plot5.starty-plot5.plotmargin,plot5.endy+plot5.plotmargin);
  linearray.add(new Linep(plot5.startx+plot5.plotmargin+gap*k,t1,plot5.startx+plot5.plotmargin+(k+1)*gap,t2,wc.get(0).get(i),wc.get(1).get(i),wc.get(2).get(i),wc.get(3).get(i)));
   (linearray.get(linearray.size()-1)).drawLine();  
   }
  if(k==1){
   t1 = map(column1.get(i),Collections.min(column1),Collections.max(column1),plot5.starty-plot5.plotmargin,plot5.endy+plot5.plotmargin);
   t2 = map(column2.get(i),Collections.min(column2),Collections.max(column2),plot5.starty-plot5.plotmargin,plot5.endy+plot5.plotmargin);
  linearray.add(new Linep(plot5.startx+plot5.plotmargin+gap*k,t1,plot5.startx+plot5.plotmargin+(k+1)*gap,t2,wc.get(0).get(i),wc.get(1).get(i),wc.get(2).get(i),wc.get(3).get(i)));
   (linearray.get(linearray.size()-1)).drawLine();  
  }
   if(k==2){
   t1 = map(column2.get(i),Collections.min(column2),Collections.max(column2),plot5.starty-plot5.plotmargin,plot5.endy+plot5.plotmargin);
   t2 = map(column3.get(i),Collections.min(column3),Collections.max(column3),plot5.starty-plot5.plotmargin,plot5.endy+plot5.plotmargin);
  linearray.add(new Linep(plot5.startx+plot5.plotmargin+gap*k,t1,plot5.startx+plot5.plotmargin+(k+1)*gap,t2,wc.get(0).get(i),wc.get(1).get(i),wc.get(2).get(i),wc.get(3).get(i)));
   (linearray.get(linearray.size()-1)).drawLine();  
  }
  
   
}