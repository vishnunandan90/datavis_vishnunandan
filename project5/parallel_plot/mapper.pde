//this method will draw the lines according to the axisinvert values 
void mapper(int k,int i){
  float temp1,temp2;
  if(k==0){
    //if axis invert is true the data is loaded in inverted direction
  if(axisinvert.get(0)){
     temp1 = map(wc.get(k).get(i),Collections.min(wc.get(k)),Collections.max(wc.get(k)),height-margin,margin);
  }else{
      temp1 = map(wc.get(k).get(i),Collections.min(wc.get(k)),Collections.max(wc.get(k)),margin,height-margin);
  }
  if(axisinvert.get(1)){
   temp2 = map(wc.get(k+1).get(i),Collections.min(wc.get(k+1)),Collections.max(wc.get(k+1)),height-margin,margin);
  }
  else{
  temp2 = map(wc.get(k+1).get(i),Collections.min(wc.get(k+1)),Collections.max(wc.get(k+1)),margin,height-margin);
  }
   linearray.add(new Line(margin+gap*k,temp1,margin+(k+1)*gap,temp2,wc.get(0).get(i),wc.get(1).get(i),wc.get(2).get(i),wc.get(3).get(i)));
   (linearray.get(linearray.size()-1)).drawLine();  
}

  if(k==1){
  if(axisinvert.get(1)){
   temp1 = map(wc.get(k).get(i),Collections.min(wc.get(k)),Collections.max(wc.get(k)),height-margin,margin);
  }
  else{
   temp1 = map(wc.get(k).get(i),Collections.min(wc.get(k)),Collections.max(wc.get(k)),margin,height-margin);
  }
  if(axisinvert.get(2)){
   temp2 = map(wc.get(k+1).get(i),Collections.min(wc.get(k+1)),Collections.max(wc.get(k+1)),height-margin,margin);
  }
  else{
   temp2 = map(wc.get(k+1).get(i),Collections.min(wc.get(k+1)),Collections.max(wc.get(k+1)),margin,height-margin);
  }
  linearray.add(new Line(margin+gap*k,temp1,margin+(k+1)*gap,temp2,wc.get(0).get(i),wc.get(1).get(i),wc.get(2).get(i),wc.get(3).get(i)));
   (linearray.get(linearray.size()-1)).drawLine();  
  }
  
  if(k==2){
   if(axisinvert.get(2)){
   temp1 = map(wc.get(k).get(i),Collections.min(wc.get(k)),Collections.max(wc.get(k)),height-margin,margin);
  }
  else{
   temp1 = map(wc.get(k).get(i),Collections.min(wc.get(k)),Collections.max(wc.get(k)),margin,height-margin);
  }
  if(axisinvert.get(3)){
   temp2 = map(wc.get(k+1).get(i),Collections.min(wc.get(k+1)),Collections.max(wc.get(k+1)),height-margin,margin);
  }
  else{
   temp2 = map(wc.get(k+1).get(i),Collections.min(wc.get(k+1)),Collections.max(wc.get(k+1)),margin,height-margin);
  }
  linearray.add(new Line(margin+gap*k,temp1,margin+(k+1)*gap,temp2,wc.get(0).get(i),wc.get(1).get(i),wc.get(2).get(i),wc.get(3).get(i)));
   (linearray.get(linearray.size()-1)).drawLine();  
  }
}