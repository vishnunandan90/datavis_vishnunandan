/* import the required packages */
import java.util.Scanner;
import java.util.Collections;
//initializing the global variables and data structures.

ArrayList<String> lines = new ArrayList<String>();
ArrayList<Float> column1 = new ArrayList<Float>();
ArrayList<Float> column2 = new ArrayList<Float>();
ArrayList<Float> column3 = new ArrayList<Float>();
ArrayList<Float> column4 = new ArrayList<Float>();
ArrayList<ArrayList<Float>> wc = new ArrayList<ArrayList<Float>>();
ArrayList<String> titles = new ArrayList<String>();
ArrayList<Line> linearray = new ArrayList<Line>();
ArrayList<Boolean> axisinvert = new ArrayList<Boolean>();
int pos1=0;
int pos2=1;
int pos3=2;
int pos4=3;
//this variable will stop the draw method execution till the data loading is done.
boolean loaddone = false;
int column_count;
float margin =100;
float gap ;
/***************************************************************************************************/
void setup(){
  /*setting size and background */
  size(800,400);
  background(255);
  gap= (width-2*margin)/3;
  selectInput("Select a data file to process:", "callbackfunction");
}
/***************************************************************************************************/
public void callbackfunction(File s) throws Exception{
  if(s == null){
    println("file not selected");
  }
  else{
    /* loading the file data into the list */
    println("you have selected " + s.getAbsolutePath());        
    Scanner sc = new Scanner(s);     
    while(sc.hasNext()){
     lines.add(sc.nextLine());
    }
    sc.close();
    
     for(int i=1;i<lines.size();i++){
     String[] pieces = lines.get(i).split(",");
    column1.add(float(pieces[0]));
    column2.add(float(pieces[1]));
    column3.add(float(pieces[2]));
    column4.add(float(pieces[3]));
    }
    wc.add(column1);
    wc.add(column2);
    wc.add(column3);
    wc.add(column4);
    
    for(int i=0;i<4;i++){
    titles.add(lines.get(0).split(",")[i]);
    }
    for(int i=0;i<4;i++){
    axisinvert.add(true);
    }    
    loaddone=true;  
    column_count = lines.get(1).split(",").length;
}  
}
/*********************************************************************************************************************/
void draw(){
   background(255);
   if(loaddone){
     drawAxis();
       for(int k=0;k<3;k++){
         for(int i=0;i<lines.size()-1;i++){     
            mapper(k,i);
          }
       }
 buttons();
 labels();
 dataDisp(mouseX,mouseY);
 linearray.removeAll(linearray);
}
}

/**********************************************************************************************************************/
void mousePressed(){
  //calls the routines and they perform the actions of swapping and inverting according to the mouseX and mouseY
  if(mouseY>=height-margin)
  { 
   swapper(mouseX,mouseY);
   axisInvert(mouseX,mouseY);
  }
}
/***********************************************************************************************************************/
//defining a class for line to store parameters of each line object.
/***********************************************************************************************************************/
class Line{
float mx;
float my;
float x1;
float x2;
float y1;
float y2;
float c1;
float c2;
float c3;
float c4;
float xvalue;
float yvalue;
Line(float x1,float y1,float x2,float y2,float c1,float c2,float c3,float c4){
this.x1=x1;
this.x2=x2;
this.y1=y1;
this.y2=y2;
this.c1=c1;
this.c2=c2;
this.c3=c3;
this.c4=c4;
}
//this method draws the line and takes the end points as input
void drawLine(){
  stroke(100);
  strokeWeight(1);
  line(x1,y1,x2,y2);
  }
}