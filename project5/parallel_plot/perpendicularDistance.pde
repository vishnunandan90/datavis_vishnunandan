//this method retruns the perpendicular distance of a mouse position with a line.
float perpendicularDistance(float x,float y, Line l){
  float diffx = l.x2-l.x1;
  float diffy = l.y2-l.y1;
  float distance = dist(l.x1,l.y1,l.x2,l.y2);
  
  float temp = (-l.x1+x)*(diffx/distance)+(-l.y1+y)*(diffy/distance);
  if(temp>0&&temp<distance){
  diffx= x-(l.x1+temp*(diffx/distance));
  diffy = y-(l.y1+temp*(diffy/distance));
  return (sqrt(diffx*diffx + diffy*diffy));
  }
  else return 1000;
}