//this method will display the corresponding data and highlight the line when mouse is over it.
void dataDisp(float mx,float my){
   int count=0;
   for(int i=0;i<linearray.size();i++){
    if(perpendicularDistance(mx,my,linearray.get(i))<0.5){
      count++;
        if(count==1){
          textSize(13);
          stroke(255,0,0);
          strokeWeight(4);
          float temp1,temp2,temp3,temp4;
          if(axisinvert.get(0)){
           temp1 = map(linearray.get(i).c1,Collections.min(wc.get(0)),Collections.max(wc.get(0)),height-margin,margin);
          }
          else{
           temp1 = map(linearray.get(i).c1,Collections.min(wc.get(0)),Collections.max(wc.get(0)),margin,height-margin);
          }
           if(axisinvert.get(1)){
           temp2 = map(linearray.get(i).c2,Collections.min(wc.get(1)),Collections.max(wc.get(1)),height-margin,margin);
          }
          else{
           temp2 = map(linearray.get(i).c2,Collections.min(wc.get(1)),Collections.max(wc.get(1)),margin,height-margin);
          }
          
           if(axisinvert.get(2)){
           temp3 = map(linearray.get(i).c3,Collections.min(wc.get(2)),Collections.max(wc.get(2)),height-margin,margin);
          }
          else{
            temp3 = map(linearray.get(i).c3,Collections.min(wc.get(2)),Collections.max(wc.get(2)),margin,height-margin);
          }
          
           if(axisinvert.get(3)){
           temp4 = map(linearray.get(i).c4,Collections.min(wc.get(3)),Collections.max(wc.get(3)),height-margin,margin);
          }
          else{
            temp4 = map(linearray.get(i).c4,Collections.min(wc.get(3)),Collections.max(wc.get(3)),margin,height-margin);
          }
          
         
           line(margin,temp1,margin+gap,temp2);
           line(margin+gap,temp2,margin+2*gap,temp3);
           line(margin+2*gap,temp3,margin+3*gap,temp4);
          fill(0);
          stroke(100);
          text(linearray.get(i).c1,margin,10+height-margin/2);
          text(linearray.get(i).c2,margin+gap,10+height-margin/2);
          text(linearray.get(i).c3,margin+2*gap,10+height-margin/2);
          text(linearray.get(i).c4,margin+3*gap,10+height-margin/2);
        }
    }
  }
}