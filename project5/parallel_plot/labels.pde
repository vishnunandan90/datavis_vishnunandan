//this method writes the labels,legends  and titles in the plot
void labels(){
for(int i=0;i<4;i++){
  if(loaddone)
  text(titles.get(i),margin+i*gap-30,height-margin+50);
}
 fill(0,255,255);
 stroke(0);
     rect(width-margin-140,margin/4,10,10);
     fill(0);
     text(" - press for axis-invert",width-margin-120,10+margin/4 );
     fill(255,0,0);
     rect(width-margin-140,20+margin/4 ,60,15);
     fill(0);
     text("- press for axis-swap", width-margin-70,35+margin/4);
     stroke(255);
     fill(255);
     textSize(15);
     text("swap",width-margin-130,33+margin/4 );
     textSize(15);
     fill(0);
     text("Parallel Plot",width/2-30,margin/2);
}