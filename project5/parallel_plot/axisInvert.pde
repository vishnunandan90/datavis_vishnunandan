//toggles the boolean if the axis invert button is pressed
void axisInvert(float mx, float my){
  
   for(int i=0;i<4;i++){
     if(mx>margin+i*gap-5 && mx<margin+i*gap+5 && my>20+height-margin && my<height-margin+30){
      axisinvert.set(i,!axisinvert.get(i));
     }
  }
 
}