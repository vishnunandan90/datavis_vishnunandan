 //this method swaps the axis when a button between them is pressed.
 void swapper(float mx, float my){
 for(int i=0;i<3;i++){
  //check if mouse pressed on a swap button and swap the axis contents if true
   if(mx<margin+gap/2+i*gap+20&&mx>margin+gap/2+i*gap-20&&my<height-margin/2+10&&my>height-margin/2){
  Collections.swap(wc,i,i+1);
  Collections.swap(titles,i,i+1);
  }}
 }