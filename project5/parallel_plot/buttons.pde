//this method draws the buttons for interaction.
void buttons(){
  for(int i=0;i<3;i++){
    fill(255,0,0);
    rect(margin-20+gap/2+i*gap-10,height-margin/2,60,15);
    fill(255);
    textSize(15);
    text("swap",margin-20+gap/2+i*gap,height-margin/2+12 );
    fill(0);
}
  for(int i=0;i<4;i++){
    //buttons for axis swap
    fill(0,255,255);
    stroke(0);
    rect(margin+i*gap-5,20+height-margin,10,10);
    fill(0);
  }
  //let us display the scale
  for(int i=0;i<4;i++){
  stroke(255,0,0);
  textSize(10);
    if(axisinvert.get(i)){
   text(Collections.min(wc.get(i)),margin+i*gap+5,height-margin+15);
   text(Collections.max(wc.get(i)),margin+i*gap+5,margin);
    }
    
    else{
   text(Collections.max(wc.get(i)),margin+i*gap+5,height-margin+15);
   text(Collections.min(wc.get(i)),margin+i*gap+5,margin);
    }
   
  }
}