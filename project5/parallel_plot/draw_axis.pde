//this method will draw the axes
void drawAxis(){
  
  float gap = (width-2*margin)/3;
  for(int i=0;i<4;i++){
    stroke(0);
    strokeWeight(2);
   line(margin+i*gap,margin-10,margin+i*gap,height-margin+10);
   ellipse(margin+i*gap,margin-10,2,2);
   ellipse(margin+i*gap,height-margin+10,2,2);
  }
 
}