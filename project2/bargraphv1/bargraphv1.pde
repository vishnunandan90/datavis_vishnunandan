//declaring the global variables
String[] lines = new String[4];
int index = 1;
int margin=70;
int multiplier;

void setup() {
  size(600,600);
  //selecting multiplier according to the canvas
  multiplier = height/120;
  //setting the background to black
  background(0);
  //now to open the dialogue box for selecting input
  selectInput("Select a file to process:", "fileSelected");
}
//call back function for selectInput 
void fileSelected(File selection) {
  if(selection==null){
    println("please choose some data file");
  }
  else{
        String filepath = selection.getAbsolutePath();
        lines = loadStrings(filepath);
       }
      }
void draw(){
  //inserting some delay to avoid nulllpointer exception for the first time of the loop
  if(index==1){
    delay(4000);
  }
  if(index < lines.length){
    //reading the strings seperated by a comma into an array 
    String[] pieces =  split(lines[index], ',');
       if(pieces[2]!=null){
        
         //calculating the heights and widths of the each bars
         float rectheight= float(pieces[2]);
         int rectwidth = (width-margin-margin)/lines.length;
         
         //coloring the bars according to the party.
         if(pieces[3].equals("DEM")){
         fill(200,100,0);
          }
       else{
       fill(100,200,0);
       }
       
    //drawing the rectangle     
     rect(index*rectwidth, height-rectheight*multiplier-margin,rectwidth,rectheight*multiplier);
 
    //inserting embellishments like titles, labellings, legends etc
     text(pieces[0],index*rectwidth+3,height-margin+20);
     text(pieces[2],index*rectwidth, height-rectheight*5-margin-5);
     fill(0, 102, 153, 51);
     text("years",(width-margin-margin)/2,height-margin+40);
     text("val1",15,(height-margin-margin)/2);
     fill(200,100,0);
     rect(width-220,20,10,10);
     text("DEM",width-200,30);
     fill(100,200,0);
     rect(width-220,40,10,10);
     text("REP",width-200,50);  
     //incrementing the index for next loop
     index = index+1;
    }
  }
}