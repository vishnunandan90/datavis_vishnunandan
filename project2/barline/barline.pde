//global values  declaration
String[] lines = new String[4];
int index = 1;
int margin=50;


void setup() {
  size(600,600);
  background(0);
  selectInput("Select a file to process:", "fileSelected");
}

void fileSelected(File selection) {
  String filepath = selection.getAbsolutePath();
  lines = loadStrings(filepath);
  }

void draw(){
  //insering delay for the first loop
  if(index==1){
    delay(3000);
  }
  
  if(index < lines.length){
  
    String[] pieces =  split(lines[index], ',');
     int rectwidth = (width-margin-margin)/lines.length;
     if(pieces[3].equals("DEM")){
         fill(200,100,0);
          }
       else{
       fill(100,200,0);
       }
     stroke(0);
     ellipse(index*rectwidth+rectwidth/2, height-float(pieces[2])*5- 2*margin,5,5);
      rect(index*rectwidth, height-float(pieces[2])*5-2*margin,rectwidth,float(pieces[2])*5);
 if(index>1){
    String[] newpieces = split(lines[index-1],',');
    stroke(255);
    
    line(index*rectwidth+rectwidth/2, height-float(pieces[2])*5- 2*margin, (index-1)*rectwidth+rectwidth/2,height-float(newpieces[2])*5- 2*margin);
    }
            
    fill(100);
    text(pieces[0],index*rectwidth+10,height-2*margin+20);
     text(pieces[2],index*rectwidth, height-float(pieces[2])*5-2*margin-5);
      fill(200,100,0);
     rect(width-220,20,10,10);
     text("DEM",width-200,30);
     fill(100,200,0);
     rect(width-220,40,10,10);
     text("REP",width-200,50);  
      text("years",(width-margin-margin)/2,height-2*margin+40);
      text("val1",15,(height-margin-margin)/2);
    index=index+1;
}
}