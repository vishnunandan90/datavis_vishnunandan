//global values  declaration
String[] lines = new String[4];
int index = 1;
int margin=50;


void setup() {
  size(600,600);
  background(0);
  selectInput("Select a file to process:", "fileSelected");
}

void fileSelected(File selection) {
  String filepath = selection.getAbsolutePath();
  lines = loadStrings(filepath);
  }

void draw(){
  //insering delay for the first loop
  if(index==1){
    delay(3000);
  }
  if(index < lines.length){
     String[] pieces =  split(lines[index], ',');
     int rectwidth = (width-margin-margin)/lines.length;
     fill(255);
     // ellipse(index*rectwidth+rectwidth/2, height-float(pieces[2])*5- margin,10,10);
      text(pieces[2],(index*rectwidth+rectwidth/2)+5, height-float(pieces[2])*5- margin+15);
  if(index<lines.length-1){
    String[] newpieces = split(lines[index+1],',');
    if(float(pieces[2]) < float(newpieces[2])){
   
     stroke(0,255,0);
    }
    else{
    stroke(255,0,0);
    }
    
    line(index*rectwidth+rectwidth/2, height-float(pieces[2])*5- margin, (index+1)*rectwidth+rectwidth/2,height-float(newpieces[2])*5- margin);
    }
    
    //embellishments
    stroke(100);
    line(index*rectwidth+rectwidth/2,height-margin,index*rectwidth+rectwidth/2,height-margin-(height-2*margin));    
   fill(100);
    text(pieces[0],index*rectwidth+10,height-margin+20);
    index=index+1;
    fill(0,255,0);
    rect(width-200,50,10,10);
    text("value1 raising",width-180,60);
     fill(255,0,0);
    rect(width-200,70,10,10);
    text("value1 falling",width-180,80);
}
}