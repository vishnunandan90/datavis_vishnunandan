float mean(ArrayList a) {
    float sum = 0;
    for (int i = 0; i < a.size(); i++) {
        sum = sum + (float)a.get(i);
    }
    return sum / a.size();
}