void corrgram(){
  Plot plot6 = new Plot(20,20,20+(1120/3),height-margin,height-margin-(height-3*margin)/2);
  plot6.createPlot();
  Plot plot7 = new Plot(20,40+(1120/3),40+2*(1120/3),height-margin,height-margin-(height-3*margin)/2);
  plot7.createPlot();
  float rwidth = (plot6.endx-plot6.endy-2*plot6.plotmargin)/3;
  float rheight = (plot6.starty-plot6.endy-2*plot6.plotmargin)/3;
  
  fill(255,0,0,1);
  
  
  
  //pearsons coefficient
  //satv vs satm
   fill((float)pearsonCoefficient(column0,column1)*100,200*(float)pearsonCoefficient(column0,column1),50*(float)pearsonCoefficient(column0,column1));
  rect(plot6.startx+plot6.plotmargin,plot6.endy+plot6.plotmargin,111.111,330/3);
  // println(pearsonCoefficient(column0,column1));
  //act vs satm
   fill((float)pearsonCoefficient(column0,column2)*100,200*(float)pearsonCoefficient(column0,column2),(float)pearsonCoefficient(column0,column1)*50);
  rect(plot6.startx+plot6.plotmargin,plot6.endy+plot6.plotmargin+330/3,111.111,330/3);
  
  //gpa vs satm
  fill((float)pearsonCoefficient(column0,column3)*100,200*(float)pearsonCoefficient(column0,column3),(float)pearsonCoefficient(column0,column3)*50);
  rect(plot6.startx+plot6.plotmargin,plot6.endy+plot6.plotmargin+2*330/3,111.111,330/3);
  //act vs satv
   fill((float)pearsonCoefficient(column1,column2)*100,200*(float)pearsonCoefficient(column1,column2),(float)pearsonCoefficient(column1,column2)*50);
  rect(plot6.startx+plot6.plotmargin+111.111,plot6.endy+plot6.plotmargin+330/3,111.111,330/3);
  //gpa vs satv
  fill((float)pearsonCoefficient(column1,column3)*100,200*(float)pearsonCoefficient(column1,column3),(float)pearsonCoefficient(column1,column3)*50);
   rect(plot6.startx+plot6.plotmargin+111.111,plot6.endy+plot6.plotmargin+2*330/3,111.111,330/3);
   //gpa vs act
    fill((float)pearsonCoefficient(column2,column3)*100,200*(float)pearsonCoefficient(column2,column3),(float)pearsonCoefficient(column2,column3));
    rect(plot6.startx+plot6.plotmargin+2*111.111,plot6.endy+plot6.plotmargin+2*330/3,111.111,330/3);
 //printing the coefficinets in boxes
    fill(255);
    
    //column1
 text((float)pearsonCoefficient(column0,column1),plot6.startx+plot6.plotmargin+30,plot6.endy+plot6.plotmargin+50);
  text((float)pearsonCoefficient(column0,column2),plot6.startx+plot6.plotmargin+30,plot6.endy+plot6.plotmargin+50+330/3);
  text((float)pearsonCoefficient(column0,column3),plot6.startx+plot6.plotmargin+30,plot6.endy+plot6.plotmargin+50+2*330/3);
 //column2
   text((float)pearsonCoefficient(column1,column2),plot6.startx+plot6.plotmargin+30+111.111,plot6.endy+plot6.plotmargin+50+330/3);
  text((float)pearsonCoefficient(column1,column3),plot6.startx+plot6.plotmargin+30+111.111,plot6.endy+plot6.plotmargin+50+2*330/3);
//column3
 text((float)pearsonCoefficient(column2,column3),plot6.startx+plot6.plotmargin+30+2*111.111,plot6.endy+plot6.plotmargin+50+2*330/3);
//spearmans coefficient
 //satv vs satm
 // fill((float)pearsonCoefficient(column0,column1)*200,200*(float)pearsonCoefficient(column0,column1),0);
   fill((float)pearsonCoefficient(column0,column1)*200,0,200*(float)pearsonCoefficient(column0,column1));
  rect(plot7.startx+plot7.plotmargin,plot7.endy+plot7.plotmargin,111.111,330/3);
  //act vs satm
   fill((float)pearsonCoefficient(column0,column2)*200,0,200*(float)pearsonCoefficient(column0,column2));
 
  rect(plot7.startx+plot7.plotmargin,plot7.endy+plot7.plotmargin+330/3,111.111,330/3);
  //gpa vs satm
    fill((float)pearsonCoefficient(column0,column3)*200,0,200*(float)pearsonCoefficient(column0,column3));
 
  rect(plot7.startx+plot7.plotmargin,plot7.endy+plot7.plotmargin+2*330/3,111.111,330/3);
  //act vs satv
    fill((float)pearsonCoefficient(column1,column2)*200,0,200*(float)pearsonCoefficient(column1,column2));
  rect(plot7.startx+plot7.plotmargin+111.111,plot7.endy+plot7.plotmargin+330/3,111.111,330/3);
  //gpa vs satv
    fill((float)pearsonCoefficient(column1,column3)*200,0,200*(float)pearsonCoefficient(column1,column3));
   rect(plot7.startx+plot7.plotmargin+111.111,plot7.endy+plot7.plotmargin+2*330/3,111.111,330/3);
   //gpa vs act
     fill((float)pearsonCoefficient(column2,column3)*200,0,200*(float)pearsonCoefficient(column2,column3));
    rect(plot7.startx+plot7.plotmargin+2*111.111,plot7.endy+plot7.plotmargin+2*330/3,111.111,330/3);
  fill(255);
text((float)pearsonCoefficient(rank(column0),rank(column1)),plot7.startx+plot7.plotmargin+30,plot7.endy+plot7.plotmargin+50);
  text((float)pearsonCoefficient(rank(column0),rank(column2)),plot7.startx+plot7.plotmargin+30,plot7.endy+plot7.plotmargin+50+330/3);
  text((float)pearsonCoefficient(rank(column0),rank(column3)),plot7.startx+plot7.plotmargin+30,plot7.endy+plot7.plotmargin+50+2*330/3);
 //column2
   text((float)pearsonCoefficient(rank(column1),rank(column2)),plot7.startx+plot7.plotmargin+30+111.111,plot7.endy+plot7.plotmargin+50+330/3);
  text((float)pearsonCoefficient(rank(column1),rank(column3)),plot7.startx+plot7.plotmargin+30+111.111,plot7.endy+plot7.plotmargin+50+2*330/3);
//column3
 text((float)pearsonCoefficient(rank(column2),rank(column3)),plot7.startx+plot7.plotmargin+30+2*111.111,plot7.endy+plot7.plotmargin+50+2*330/3);
}