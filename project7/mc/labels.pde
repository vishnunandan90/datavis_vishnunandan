
void labels(){
   Plot plot5 = new Plot(20,3*margin+2*(1120/3),1180,height-margin,height-margin-(height-3*margin)/2);
   Plot plot7 = new Plot(20,40+(1120/3),40+2*(1120/3),height-margin,height-margin-(height-3*margin)/2);
  Plot plot1 = new Plot(20,20,20+(1120/3),390,20);
   Plot plot2 = new Plot(20,40+(1120/3),40+2*(1120/3),390,20);
   Plot plot3 = new Plot(20,3*margin+2*(1120/3),3*margin+3*(1120/3),390,20);
  // Plot plot5 = new Plot(10,610,1180,height-margin,height-margin-(height-3*margin)/2);
 // rotate(HALF_PI);
 // Plot plot5 = new Plot(10,610,1180,height-margin,height-margin-(height-3*margin)/2);
  float gap = ((plot5.endx-plot5.startx)-2*plot5.plotmargin)/3;
 textSize(15);
 fill(100,0,0);
 text("DASHBOARD",width/2-10,15);
 textSize(10);
pushMatrix();
fill(200,50,70);
  translate(15,250);
  rotate(1.5*PI);
   translate(-15,-250);
  text(labely,15,250);
//   translate(-x,-y);
  popMatrix(); 


text(labelx,150,height/2-15);
pushMatrix();
fill(200,50,70);
  translate(410,250);
  rotate(1.5*PI);
   translate(-410,-250);
  text(labely,410,250);
//   translate(-x,-y);
  popMatrix(); 


text(labelx,545,height/2-15);
pushMatrix();
fill(200,50,70);
  translate(805,250);
  rotate(1.5*PI);
   translate(-805,-250);
  text(labely,805,250);
//   translate(-x,-y);
  popMatrix(); 
text(labelx,940,height/2-15);
for(int i=0;i<4;i++){
  
//  text(titles.get(i),plot5.startx+plot5.plotmargin+i*gap,plot5.endy+plot5.plotmargin);
//text(Math.round(Collections.max(wc.get(i))),plot5.startx+plot5.plotmargin+i*gap-20,plot5.endy+plot5.plotmargin);
////plot5.startx+plot5.plotmargin+i*gap,plot5.starty-plot5.plotmargin
//text(Math.round(Collections.min(wc.get(i))),plot5.startx+plot5.plotmargin+i*gap,plot5.starty-plot5.plotmargin+7);
//text("PARALLEL PLOT",850,790);
pushMatrix();
fill(200,50,70);
  translate(30,510);
  rotate(1.5*PI);
   translate(-30,-510);
text(titles.get(1),30,510);
//   translate(-x,-y);
  popMatrix(); 
  
  pushMatrix();
fill(200,50,70);
  translate(430,510);
  rotate(1.5*PI);
   translate(-430,-510);
text(titles.get(1),430,510);
//   translate(-x,-y);
  popMatrix(); 
  
  pushMatrix();
fill(200,50,70);
  translate(30,620);
  rotate(1.5*PI);
   translate(-30,-620);
text(titles.get(2),30,620);
//   translate(-x,-y);
  popMatrix();
  
   pushMatrix();
fill(200,50,70);
  translate(430,620);
  rotate(1.5*PI);
   translate(-430,-620);
text(titles.get(2),430,620);
//   translate(-x,-y);
  popMatrix();
 pushMatrix();
fill(200,50,70);
  translate(30,730);
  rotate(1.5*PI);
   translate(-30,-730);
text(titles.get(3),30,730);
//   translate(-x,-y);
  popMatrix(); 
 pushMatrix();
 translate(430,730);
  rotate(1.5*PI);
   translate(-430,-730);
text(titles.get(3),430,730);
//   translate(-x,-y);
  popMatrix(); 
 
 Plot plot6 = new Plot(20,20,20+(1120/3),height-margin,height-margin-(height-3*margin)/2);
 
text(titles.get(0),plot6.startx+plot6.plotmargin+50,height-25);
text(titles.get(1),plot6.startx+plot6.plotmargin+111.111+50,height-25);
text(titles.get(2),plot6.startx+plot6.plotmargin+2*111.111+50,height-25);

text(titles.get(0),400+plot6.startx+plot6.plotmargin+50,height-25);
text(titles.get(1),400+plot6.startx+plot6.plotmargin+111.111+50,height-25);
text(titles.get(2),400+plot6.startx+plot6.plotmargin+2*111.111+50,height-25);


text(Math.round((Collections.min(temp2))),plot1.startx-15,plot1.starty-plot1.plotmargin);
text(Math.round(Collections.max(temp2)),400+plot1.startx-20,plot1.endy+plot1.plotmargin);
text(Math.round(Collections.min(temp2)),400+plot1.startx-20,plot1.starty-plot1.plotmargin);
text(Math.round(Collections.max(temp2)),plot1.startx-15,plot1.endy+plot1.plotmargin);
text(0,800+plot1.startx-20,plot1.starty-plot1.plotmargin);
text(Math.round(Collections.max(temp2)),800+plot1.startx-25,plot1.endy+plot1.plotmargin);
textSize(15);
text("bargraph",plot1.startx+120,plot1.endy+15);
text("linegraph",plot1.startx+520,plot1.endy+15);
text("scatter plot",plot1.startx+900,plot1.endy+15);
//text("scatter matrix",plot1.startx+200,plot1.endy+405);


textSize(15);
stroke(0);
text("for changing axis press a,b,c,d,e,f",30,15);
textSize(15);
text("hover the mouse over graphs for interaction between graphs", width-450,10);
textSize(10);
textSize(15);
text("spearmans rank coefficient matrix", plot7.startx+50,plot7.endy);
text("pearsons coefficient matrix", plot6.startx+50,plot6.endy);
text("histogram for" + labely,plot5.startx+80,plot5.endy);
textSize(10);
text("press up arrow and down arrow to change number of bins from 2 to 15",plot5.startx,plot5.endy+15);
pushMatrix();
translate(plot5.startx-5,plot5.endy+180);
  rotate(1.5*PI);
   translate(-plot5.startx+5,-plot5.endy-180);
text("frequency",plot5.startx-5,plot5.endy+180);
//   translate(-x,-y);
  popMatrix(); 


}

}