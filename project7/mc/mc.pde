/* import the required packages */
import java.util.Scanner;
import java.util.Collections;

ArrayList<String> lines = new ArrayList<String>();
ArrayList<Float> column0 = new ArrayList<Float>();
ArrayList<Float> column1 = new ArrayList<Float>();
ArrayList<Float> column2 = new ArrayList<Float>();
ArrayList<Float> column3 = new ArrayList<Float>();
//ArrayList<Linep> linearray = new ArrayList<Linep>();
ArrayList<Float> temp1 = new ArrayList<Float>();
ArrayList<Float> temp2 = new ArrayList<Float>();
ArrayList<ArrayList<Float>> wc = new ArrayList<ArrayList<Float>>();
ArrayList<String> titles = new ArrayList<String>();
ArrayList<Float> a1 = new ArrayList<Float>();
ArrayList<Float> a2 = new ArrayList<Float>();
 int bins = 15;

String labely;
String labelx;
float margin = 20;
float mx,my;
boolean loaddone = false;
/***************************************************************************************************/
void setup(){
  /*setting size and background */
  size(1200,800);
  background(255);
  selectInput("Select a data file to process:", "callbackfunction");
   temp1 = column1;
        temp2 = column2; 
       
}

public void callbackfunction(File s) throws Exception{
  if(s == null){
    println("file not selected");
  }
  else{
 

    /* loading the file data into the list */

    println("you have selected " + s.getAbsolutePath());        
    Scanner sc = new Scanner(s);     
    while(sc.hasNext()){
     lines.add(sc.nextLine());
    }
    sc.close();
    /*loading the data intlo each columns */
    for(int i=1;i<lines.size();i++){
     String[] pieces = lines.get(i).split(",");
    column0.add(float(pieces[0]));
    column1.add(float(pieces[1]));
    column2.add(float(pieces[2]));
    column3.add(float(pieces[3]));
    }
    wc.add(column0);
    wc.add(column1);
    wc.add(column2);
    wc.add(column3);
    /*loading the titles into titles array*/
    for(int i=0;i<4;i++){
    titles.add(lines.get(0).split(",")[i]);
    }
     labelx = titles.get(1);
  labely = titles.get(2);
    loaddone = true;
  }
}    
   void draw(){
     background(255);
   if(loaddone){
     //drawBarGraph(wc.get(0),wc.get(1));
      for(int i=1;i<lines.size()-1;i++){
            new Bar(temp1.get(i),temp2.get(i),Collections.min(temp1),Collections.max(temp1),Collections.min(temp2),Collections.max(temp2),column0.get(i),column1.get(i),column2.get(i),column3.get(i),i).drawBarGraph();
           new Dot(temp1.get(i),temp2.get(i),Collections.min(temp1),Collections.max(temp1),Collections.min(temp2),Collections.max(temp2),column0.get(i),column1.get(i),column2.get(i),column3.get(i),i).drawDot();
           if(i!=lines.size()-2)
            new Line(temp1.get(i),temp2.get(i),temp2.get(i+1),Collections.min(temp1),Collections.max(temp1),Collections.min(temp2),Collections.max(temp2),column0.get(i),column1.get(i),column2.get(i),column3.get(i),i).drawLine();  
     histogram();
     //ArrayList<Float> rankarray1 = new ArrayList<Float>();
     //ArrayList<Float> rankarray2 = new ArrayList<Float>();
     
   //  rankarray1 = rank(temp1);
     //rankarray2 = rank(temp2);
    
    // println(rank(rankarray1));
    // println(rank(rankarray1));
     //println(rankarray1);
     //rankarray1 = rank(temp1);
     //rankarray2 = rank(temp2);
     corrgram();
    // println("satm mean" + standardDeviation(column0));
    // println(rankarray2);
    //println("expe" + covariance(column0,column1));
     
}

  //drawAxis();
 //drawScatterMatrix();
 
  //for(int k=0;k<3;k++){
  //       for(int i=0;i<lines.size()-1;i++){     
  //        //  mapper(k,i);
  //        }
  //     }
         //dataDisp(mouseX,mouseY);
  clicked(mouseX,mouseY);
  labels();
  
  
 // pearsonCoefficient(temp1,temp2);
//println(pearsonCoefficient(temp1,temp2));
   }
   }
   /* class for bar in bargraph */
  class Bar{
     float cx,cy,c0,c1,c2,c3,maxx,maxy,minx,miny;
     int i;
   Bar(float cx, float cy,float minx,float maxx,float miny,float maxy, float c0, float c1, float c2, float c3,int i){
   this.cx = cx;
   this.cy = cy;
   this.c0= c0;
   this.c1= c1;
   this.c2= c2;
   this.c3= c3;
   this.minx=minx;
   this.miny=miny;
   this.maxy=maxy;
   this.maxx=maxx;
   this.i = i;
   }
   /*takes the particular row value and draws the bar */
  void drawBarGraph(){   
     Plot plot1 = new Plot(20,20,20+(1120/3),390,20);
     float barwidth = (plot1.endx-plot1.startx-2*plot1.plotmargin)/lines.size();
     float x = map(cy,miny,maxy,0,330);
     stroke(150);
     fill(150,150,150); 
     rect(plot1.plotmargin+margin+(i-1)*barwidth,plot1.starty-plot1.plotmargin-x,barwidth,x);
     plot1.createPlot();
  }
}

/*scatter plots */
class Dot{
float cx,cy,c0,c1,c2,c3,maxx,maxy,minx,miny;
     int i;
Dot(float cx, float cy,float minx,float maxx,float miny,float maxy, float c0, float c1, float c2, float c3,int i){
   this.cx = cx;
   this.cy = cy;
   this.c0= c0;
   this.c1= c1;
   this.c2= c2;
   this.c3= c3;
   this.minx=minx;
   this.miny=miny;
   this.maxy=maxy;
   this.maxx=maxx;
   this.i = i;
}
void drawDot(){
  Plot plot3 = new Plot(20,3*margin+2*(1120/3),3*margin+3*(1120/3),390,20);
  float x = map(cx, 0,maxx,0,(1120/3)-2*plot3.plotmargin);
  float y = map(cy,0,maxy,330,0);
  fill(0);
  ellipse(x+3*margin+2*((width-4*margin)/3),y+plot3.plotmargin+margin,3,3);
  plot3.createPlot();
}
}


/*line class */

class Line{
   float cx,cy,c0,c1,c2,c3,cy2,maxx,maxy,minx,miny;
     int i;
Line(float cx, float cy,float cy2,float minx,float maxx,float miny,float maxy, float c0, float c1, float c2, float c3,int i){
   this.cx = cx;
   this.cy = cy;
   this.c0= c0;
   this.c1= c1;
   this.c2= c2;
   this.c3= c3;
   this.cy2=cy2;
   this.minx=minx;
   this.miny=miny;
   this.maxy=maxy;
   this.maxx=maxx;
   this.i = i;

}
void drawLine(){
      Plot plot2 = new Plot(20,40+(1120/3),40+2*(1120/3),390,20);
      float barwidth = (plot2.endx-plot2.startx-2*plot2.plotmargin)/lines.size();
      float x1 = map(cy,miny,maxy,0,330);
      ellipse(plot2.startx+plot2.plotmargin+(i-1)*barwidth,plot2.starty-plot2.plotmargin-x1,2,2);
     float x2 = map(cy2,miny,maxy,0,330);
      if(i!=lines.size()){
      
      line(plot2.startx+plot2.plotmargin+(i-1)*barwidth,plot2.starty-plot2.plotmargin-x1,plot2.startx+plot2.plotmargin+(i)*barwidth,plot2.starty-plot2.plotmargin-x2);
      }
      else{
      ellipse(plot2.startx+plot2.plotmargin+(i)*barwidth,plot2.starty-plot2.plotmargin- x2,2,2);
      }
      plot2.createPlot();
}
}



void keyPressed(){
  if(key=='a'){
  temp1 = column0;
  temp2 = column1;
  labelx = titles.get(0);
  labely = titles.get(1);
  }
   if(key=='b'){
  temp1 = column0;
  temp2 = column2;
   labelx = titles.get(0);
  labely = titles.get(2);
  }
  if(key=='c'){
  temp1 = column0;
  temp2 = column3;
   labelx = titles.get(0);
  labely = titles.get(3);
  }
  if(key=='d'){
  temp1 = column1;
  temp2 = column2;
   labelx = titles.get(1);
  labely = titles.get(2);
  }
   if(key=='e'){
  temp1 = column1;
  temp2 = column3;
  labelx = titles.get(1);
  labely = titles.get(3);
  }
  if(key=='f'){
  temp1 = column2;
  temp2 = column3;
   labelx = titles.get(2);
  labely = titles.get(3);
  }
  
  if(keyCode == UP && bins<=14){
  bins++;
  }
  if(keyCode == DOWN && bins>=2){
  bins--;
  }
  
  
}
 //void drawScatterMatrix(){
 //    Plot plot4 = new Plot(20,margin,40+2*(1120/3),height-margin,height-margin-(height-3*margin)/2);
 //    plot4.createPlot();
 //    float boxwidth= (plot4.endx-plot4.startx-2*plot4.plotmargin)/3;
 //    float boxheight = (plot4.starty-plot4.endy-2*2*plot4.plotmargin)/3;
 //    Plot plot41 = new Plot(5,plot4.startx+plot4.plotmargin,plot4.startx+plot4.plotmargin+boxwidth,plot4.plotmargin+plot4.endy+boxheight,plot4.plotmargin+plot4.endy);
 //    Plot plot42 = new Plot(5,plot4.startx+plot4.plotmargin,plot4.startx+plot4.plotmargin+boxwidth,plot4.plotmargin+plot4.endy+2*boxheight,plot4.plotmargin+plot4.endy+boxheight);
 //    Plot plot43 = new Plot(5,plot4.startx+plot4.plotmargin+boxwidth,plot4.startx+plot4.plotmargin+2*boxwidth,plot4.plotmargin+plot4.endy+2*boxheight,plot4.plotmargin+plot4.endy+boxheight);
 //    Plot plot44 = new Plot(5,plot4.startx+plot4.plotmargin,plot4.startx+plot4.plotmargin+boxwidth,plot4.plotmargin+plot4.endy+3*boxheight,plot4.plotmargin+plot4.endy+2*boxheight);
 //    Plot plot45 = new Plot(5,plot4.startx+plot4.plotmargin+boxwidth,plot4.startx+plot4.plotmargin+2*boxwidth,plot4.plotmargin+plot4.endy+3*boxheight,plot4.plotmargin+plot4.endy+2*boxheight);
 //    Plot plot46 = new Plot(5,plot4.startx+plot4.plotmargin+2*boxwidth,plot4.startx+plot4.plotmargin+3*boxwidth,plot4.plotmargin+plot4.endy+3*boxheight,plot4.plotmargin+plot4.endy+2*boxheight);
 //    plot46.createPlot(); 
 //    plot45.createPlot(); 
 //    plot44.createPlot(); 
 //    plot43.createPlot(); 
 //    plot42.createPlot();
 //    plot41.createPlot();
   
 //  for(int i=1;i<lines.size()-1;i++){
 //  float x= map(column0.get(i),0,Collections.max(column0),0,boxwidth-10);
 //   float y= map(column1.get(i),0,Collections.max(column1),0,boxheight-10);
 //    ellipse(plot41.startx+plot41.plotmargin+x,plot41.starty-plot41.plotmargin-y,1,1);  
     
 //      x= map(column0.get(i),0,Collections.max(column0),0,boxwidth-10);
 //      y= map(column2.get(i),0,Collections.max(column2),0,boxheight-10);
 //       ellipse(plot41.startx+plot41.plotmargin+x,plot41.starty-plot41.plotmargin-y+boxheight,1,1);  
 //     x= map(column0.get(i),0,Collections.max(column0),0,boxwidth-10);
 //      y= map(column3.get(i),0,Collections.max(column3),0,boxheight-10);
 //       ellipse(plot41.startx+plot41.plotmargin+x,plot41.starty-plot41.plotmargin-y+2*boxheight,1,1);  
 //        x= map(column1.get(i),0,Collections.max(column1),0,boxwidth-10);
 //      y= map(column2.get(i),0,Collections.max(column2),0,boxheight-10);
 //       ellipse(plot42.startx+plot42.plotmargin+x+boxwidth,plot42.starty-plot42.plotmargin-y,1,1);  
 //        x= map(column1.get(i),0,Collections.max(column1),0,boxwidth-10);
 //      y= map(column3.get(i),0,Collections.max(column3),0,boxheight-10);
 //       ellipse(plot42.startx+plot42.plotmargin+x+boxwidth,plot42.starty-plot42.plotmargin-y+boxheight,1,1);  
 //       x= map(column2.get(i),0,Collections.max(column2),0,boxwidth-10);
 //      y= map(column3.get(i),0,Collections.max(column3),0,boxheight-10);
 //       ellipse(plot42.startx+plot42.plotmargin+x+2*boxwidth,plot42.starty-plot42.plotmargin-y+boxheight,1,1);    
 //}
   
   
 //  }
   
   class Plot{
   float plotmargin,startx,endx,starty,endy;
   Plot(float plotmargin,float startx,float endx,float starty,float endy){
   this.plotmargin = plotmargin;
   this.startx = startx;
   this.endx = endx;
   this.starty = starty;
   this.endy = endy;
   }
   void createPlot(){
   line(startx,starty,startx,endy);
   line(startx,endy,endx,endy);
   line(endx,endy,endx,starty);
   line(endx,starty,startx,starty);
   }
   
   }


  