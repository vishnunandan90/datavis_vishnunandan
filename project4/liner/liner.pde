import java.util.Scanner;
import java.util.Collections;
import java.lang.Integer;
import java.lang.Double;
 ArrayList<Float> temp2 = new ArrayList<Float>();

ArrayList<String> lines = new ArrayList<String>();
float margin = 100;
boolean keyp=false;
boolean lgdrawn = false;

void setup(){
  /*setting size and background */
  size(800,600);
  background(0);
  selectInput("Select a data file to process:", "callbackfunction");
}

public void callbackfunction(File s) throws Exception{
  if(s == null){
    println("file not selected");
  }
  else{
    /* loading the file data into the list */
    println("you have selected " + s.getAbsolutePath());        
    Scanner sc = new Scanner(s);     
    while(sc.hasNext()){
     lines.add(sc.next());
    }
    sc.close();
  
  }
}
void draw(){
  background(0);

if(keyp){
  clear();
  background(0);
drawlinegraph(Character.getNumericValue(key));
keyp = false;
 rectwidth =  (width-2*margin)/(lines.size()-1);
}
if(lgdrawn){
datadisp();
drawlinegraph(Character.getNumericValue(key));
}
 if(!lgdrawn){
   textSize(15);
   
 text("press 1 for VALUE0",width-150,margin/4);
 text("press 2 for VALUE!",width-150,margin/4+20);
 text("press 0 for YEAR",width-150,margin/4+40);
 //text("press 0 for " + lines.get(0).split(",")[0],width/2,margin+20);
 //drawlinegraph(1);
  }
   textSize(15);
   fill(255);
 text("press 1 for VALUE0",width-150,margin/4);
 text("press 2 for VALUE!",width-150,margin/4+20);
 text("press 0 for YEAR",width-150,margin/4+40);
}


void keyPressed(){
dataloader(Character.getNumericValue(key));

 } 
 