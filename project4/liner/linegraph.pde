float rectwidth; 
void drawlinegraph(int x){
  rectwidth =  (width-2*margin)/(lines.size()-1);
  for(int l=0;l<lines.size()-1;l++){
    stroke(255);
    float prev=0;
    if(l!=0){
      prev =map(temp2.get(l-1),Collections.min(temp2),1+Collections.max(temp2),margin,height-2*margin);
    }
   float y = map(temp2.get(l),Collections.min(temp2),1+Collections.max(temp2),margin,height-2*margin);
   //rect(margin+((l)*rectwidth), height-margin-y,rectwidth,y);
   if(l!=0){
   line(margin+((l)*rectwidth)+rectwidth/2, height-margin-y,margin+((l-1)*rectwidth)+rectwidth/2,height-margin-prev);
 }
   fill(200,0,0);
   ellipse(margin+((l)*rectwidth)+rectwidth/2, height-margin-y,5,5);
   line(margin+((l)*rectwidth)+rectwidth/2,margin,margin+((l)*rectwidth)+rectwidth/2,height-margin);
   textSize(10);
   text(temp2.get(l),margin+((l)*rectwidth)+10, height-margin-y+20);
 }  
   textSize(20);
   text(lines.get(0).split(",")[x],width/2-20,margin/2);
   lgdrawn=true;
}