/*import statements */
import java.util.Scanner;
boolean spcompleted = false;
//global declarations
ArrayList<String> lines = new ArrayList<String>();
/* dimensions for axes */
int margin = 100;
float xmax,ymax,xgapvalue,ygapvalue;
/********************************************************************
*********setup function runs first***********************************
*********************************************************************/
//void settings(){
//size(800,600);
//}

void setup(){
  size(800,600);
  background(0);
  selectInput("Select a data file to process:", "callbackfunction");
}
/*callback function if file selected or window closed */
public void callbackfunction(File s) throws Exception{
  if(s == null){
    println("file not selected");
  }
  else{
    println("you have selected " + s.getAbsolutePath());
    Scanner sc = new Scanner(s);     
    while(sc.hasNext()){
      lines.add(sc.next());
    }
   sc.close();
  }
 
}
int count=0;
void draw(){
 //background(0);
  if(count==0){ 
  delay(3000);
  }
  textSize(15);
  fill(255);
   text("a-satv vs satm",20,25);
   text("b-act vs satm",20,45); 
   text("c-gpa vs satm",20,65);
   text("d-act vs satv",150,25);
   text("e-gpa vs satv",150,45);
   text("f-gpa vs act",150,65);
    count++;  /* initializing values for axes */
    datapop();
   
  }
  
void keyPressed(){
 // delay(3000);
fill(100,100,0);
  //a - satv vs satm
if(key=='a'){
  clear();
 // println("a");
drawscatter(1,0);
//datapop(1,0);
 textSize(25);
text("satv vs satm",width/2,margin/2);
textSize(15);
text("satv",width/2,height-margin/4);
text("satm",margin/8,height/2);
}
//b-act vs satm
if(key=='b'){
  clear();
  //println("b");
drawscatter(2,0);
//datapop(2,0);
 textSize(25);
text("act vs satm",width/2,margin/2);
text("act",width/2,height-margin/4);
text("satm",margin/8,height/2);
}
//c- gpa vs satm
if(key=='c'){
   clear();
  //println("c");
  drawscatter(3,0);
  //datapop(3,0);
   textSize(25);
  text("gpa vs satm",width/2,margin/2);
  text("gpa",width/2,height-margin/4);
text("satm",margin/8,height/2);
}
//d- act vs satv
if(key=='d'){
   clear();
  //println("d");
  drawscatter(2,1);
 // datapop(2,1);
   textSize(25);
  text("act vs satv",width/2,margin/2);
  text("act",width/2,height-margin/4);
text("satv",margin/8,height/2);
}
//e- gpa vs satv
if(key=='e'){
   clear();
  //println("e");
  drawscatter(3,1);
 // datapop(3,1);
   textSize(25);
   text("gpa vs satv",width/2,margin/2);
   text("gpa",width/2,height-margin/4);
text("satv",margin/8,height/2);
}
//f - gpa vs act
if(key=='f'){
   clear();
  //println("f");
  drawscatter(3,2);
 // datapop(3,2);
   textSize(25);
  text(" gpa vs act",width/2,margin/2);
text("gpa",width/2,height-margin/4);
text("act",margin/8,height/2);
}
}