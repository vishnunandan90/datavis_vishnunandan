int c=1;
ArrayList<Float>  xarray = new ArrayList<Float>();
ArrayList<Float>  yarray = new ArrayList<Float>();

boolean spdrawn=false;
void drawscatter(int a, int b){
  clear();
  background(0);

  int i =1;
  while(i<lines.size()){    
    String[] pieces = lines.get(i).split(",");
   // println("from unmapped:"+float(pieces[a]));
    float x = map(float(pieces[a]),0,max(lines,a),margin,width-margin);
    xarray.add(x);
     spdrawn =true;
   
    //float t2 = map(x,margin,width-margin,0,max(lines,a));
   // float("from remapped "+ t2);
    if(float(pieces[a])==max(lines,a)){
      xmax=x;
      
    } 
   
    float y = map(float(pieces[b]),0,max(lines,b),height-margin,margin);
    yarray.add(y);
    if(float(pieces[b])==max(lines,b)){
      ymax=y;
    }
     
    xgapvalue = max(lines,a)/10;
    ygapvalue = max(lines,b)/10;
    /* calling function for drawing axes */
    
    drawaxis(xmax,xgapvalue,ymax,ygapvalue);
    fill(255);
    ellipse(x,y,3,3);
    i++;
   }
   spcompleted =true;

}