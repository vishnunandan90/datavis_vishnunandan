import java.util.Scanner;
import java.util.Collections;
import java.lang.Integer;
import java.lang.Double;
 
 ArrayList<Float> temp2 = new ArrayList<Float>();

ArrayList<String> lines = new ArrayList<String>();
float margin = 100;
boolean keyp=false;
boolean bgdrawn=false;

void setup(){
  /*setting size and background */
  size(800,600);
  background(0);
  selectInput("Select a data file to process:", "callbackfunction");
}

public void callbackfunction(File s) throws Exception{
  if(s == null){
    println("file not selected");
  }
  else{
    /* loading the file data into the list */
    println("you have selected " + s.getAbsolutePath());        
    Scanner sc = new Scanner(s);     
    while(sc.hasNext()){
     lines.add(sc.next());
    }
    sc.close();
  
  }
}
void draw(){
 background(0);
   // rect(10,10,100,50);
if(keyp){
  clear();
  background(0);
  drawbargraph(Character.getNumericValue(key));
   labels();
  keyp = false;
  rectwidth =  (width-2*margin)/(lines.size()-1);
  }
  if(bgdrawn){
   
  datadisp1();
  drawbargraph(Character.getNumericValue(key));
   labels();
  }
  if(!bgdrawn){
    textSize(15);
 text("press1 for VALUE0",width-margin-100,margin/4);
 text("press2 for VALUE1",width-margin-100,margin/4+20);
 text("press0 for YEAR",width-margin-100,margin/4+40);
  }

}
void keyPressed(){
dataloader(Character.getNumericValue(key));
 } 




 