/* draws the bargraph acc to values in the arraylist */
float rectwidth;
void drawbargraph(int x){
  //background(255);
  rectwidth =  (width-2*margin)/(lines.size()-1);
 
  for(int l=0;l<lines.size()-1;l++){
   
   float y = map(temp2.get(l),Collections.min(temp2),1+Collections.max(temp2),margin,height-2*margin);
   
   rect(margin+((l)*rectwidth), height-margin-y,rectwidth,y);
   text(temp2.get(l),margin+((l)*rectwidth)+10, height-margin-y-20);
 }
   text(lines.get(0).split(",")[x],width/2,height-margin/2);
   bgdrawn=true;
}