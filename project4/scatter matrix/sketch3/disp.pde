void disp(){

int i =1;
  while(i<lines.size()){    
    pieces = lines.get(i).split(",");
    /* initializing the width and height of each box */
   
    /*colouring the eclipses */
    fill(255,255,0);
    /* mapping the attributes to the dimensions using the range and plotting them in respective boxes in the matrix*/
   //00
   if(!dispdone){
     fill(255,255,0);
     stroke(255,255,0);
    x = map(float(pieces[0]),0,max(lines,0),margin, margin+xspacer);
    y = map(float(pieces[1]),0,max(lines,1),margin+yspacer,margin);   
    ellipse(x,y+innermargin+yspacer,2,2);
   
    x = map(float(pieces[0]),0,max(lines,0),margin, margin+xspacer);
    y = map(float(pieces[2]),0,max(lines,2),margin+yspacer,margin);   
    ellipse(x,y+2*innermargin+2*yspacer,2,2);
      
    x = map(float(pieces[1]),0,max(lines,1),margin, margin+xspacer);
    y = map(float(pieces[2]),0,max(lines,2),margin+yspacer,margin);   
    ellipse(x+innermargin+xspacer,y+2*innermargin+2*yspacer,2,2);
   
   
   
    x = map(float(pieces[0]),0,max(lines,0),margin, margin+xspacer);
    y = map(float(pieces[3]),0,max(lines,3),margin+yspacer,margin);   
    ellipse(x,y+3*innermargin+3*yspacer,2,2);
   
    x = map(float(pieces[1]),0,max(lines,1),margin, margin+xspacer);
    y = map(float(pieces[3]),0,max(lines,3),margin+yspacer,margin);   
    ellipse(x+innermargin+xspacer,y+3*innermargin+3*yspacer,2,2);
   
    x = map(float(pieces[2]),0,max(lines,2),margin, margin+xspacer);
    y = map(float(pieces[3]),0,max(lines,3),margin+yspacer,margin);   
    ellipse(x+2*innermargin+2*xspacer,y+3*innermargin+3*yspacer,2,2);
   
    x = map(float(pieces[2]),0,max(lines,2),margin, margin+xspacer);
    y = map(float(pieces[3]),0,max(lines,3),margin+yspacer,margin);   
    ellipse(x+2*innermargin+2*xspacer,y+3*innermargin+3*yspacer,2,2);     
     
   }
    
    i++;
    }
   
    /* calling function to insert matrix and labels */
    if(i==lines.size()){
     labelsandmatrix();
    }  
  
  

}