//import statements
import java.util.Scanner;
//global declarations
ArrayList<String> lines = new ArrayList<String>();
float x,y,xspacer,yspacer;
//dimensions
int margin = 70;
int innermargin = 10;
int count=0;
String[] pieces;
boolean dispdone=false;
   
//set up method will be running first
void setup(){
  size(800,600);
  background(0);
  xspacer = (width-2*margin-innermargin*3)/4; 
  yspacer = (height-2*margin-3*innermargin)/4; 
  selectInput("Select a data file to process:", "callbackfunction");
}
/***********************************************************************/
/*call back function after selecting a file or cloasing the window */
/***********************************************************************/
public void callbackfunction(File s) throws Exception{
  if(s == null){
    println("file not selected");
  }
  else{
        /*file is selected so loading the string into the list */
        println("you have selected " + s.getAbsolutePath());  
        Scanner sc = new Scanner(s); 
        while(sc.hasNext()){
          lines.add(sc.next());
        }
        sc.close();  
  }
}

/*************************************************************************/
/* draw method */
/*************************************************************************/
void draw(){
  //println(count);
  
  if(count==0){
  /* delay to start delay after loading the list and avoid null pointer exception */
  delay(3000); 
   disp();
  count++;/* making the draw loop execute only once */
  }    
}

void mousePressed(){ 
 clear();
 background(0);

  if((margin-innermargin<mouseX)&&(mouseX<margin+xspacer+innermargin/2)){
  if((margin+yspacer+innermargin/2<mouseY)&&(mouseY<margin+2*yspacer+3*innermargin/2)){
   displaybox(0,1);
}
  if((mouseY>margin+2*yspacer+3*innermargin/2)&&(mouseY<margin+3*yspacer+5*innermargin/2)){
     displaybox(0,2);
}
   if((mouseY>margin+2*yspacer+3*innermargin/2)&&(mouseY<margin+3*yspacer+5*innermargin/2)){
     displaybox(0,2);
}  
    if((mouseY>margin+3*yspacer+5*innermargin/2)&&(mouseY<height-margin+innermargin)){
    displaybox(0,3);
    }
}

  if((mouseX>margin+xspacer+innermargin/2)&&(mouseX<margin+2*xspacer+3*innermargin/2)){
   if((mouseY>margin+2*yspacer+3*innermargin/2)&&(mouseY<margin+3*yspacer+5*innermargin/2)){
     displaybox(1,2);
  }
   if((mouseY>margin+3*yspacer+5*innermargin/2)&&(mouseY<height-margin+innermargin)){
    displaybox(1,3);
    }
  
  }
  
  if((mouseX>margin+2*xspacer+3*innermargin/2)&&(mouseX<margin+3*xspacer+5*innermargin/2)){
   if((mouseY>margin+3*yspacer+5*innermargin/2)&&(mouseY<height-margin+innermargin)){
   displaybox(2,3);
    }}
  
  



  }