public void labelsandmatrix(){
  stroke(100,100,100);
  /* labels*/
    for(int g=0;g<3;g++){    
    String[] temp = lines.get(0).split(",");
    text(temp[g],margin+xspacer/2+g*(xspacer+innermargin),height-margin/4);
    text(temp[g+1],margin/10,margin+yspacer/2+g*(innermargin+yspacer)+yspacer);
  }
  
    
    /*matrix */
   //h1
   line(margin-innermargin,margin+yspacer+innermargin/2,margin+xspacer+innermargin/2,margin+yspacer+innermargin/2);
    //horizontal2
    line(margin-innermargin,margin+2*yspacer+3*innermargin/2,margin+2*xspacer+3*innermargin/2,margin+2*yspacer+3*innermargin/2);
    //h3
    line(margin-innermargin,margin+3*yspacer+5*innermargin/2,margin+3*xspacer+5*innermargin/2,margin+3*yspacer+5*innermargin/2);
     //v2
     line(margin+xspacer+innermargin/2,margin+yspacer+innermargin/2,margin+xspacer+innermargin/2,height-margin+innermargin);
     
     //v3
     line(margin+2*xspacer+3*innermargin/2,margin+2*yspacer+3*innermargin/2,margin+2*xspacer+3*innermargin/2,height-margin+innermargin);
     //v4
     line(margin+3*xspacer+5*innermargin/2,margin+3*yspacer+5*innermargin/2,margin+3*xspacer+5*innermargin/2,height-margin+innermargin);
   
  //v1
  line(margin-innermargin,margin+yspacer+innermargin/2,margin-innermargin,height-margin+innermargin);
     
     //h4
     line(margin-innermargin,height-margin+innermargin,margin+3*xspacer+5*innermargin/2,height-margin+innermargin);
     
     /* display box */
     stroke(255);
   //v1
   line(margin+2*xspacer+3*innermargin/2,margin-innermargin,margin+2*xspacer+3*innermargin/2,margin+2*yspacer+3*innermargin/2);
   //h2
   line(margin+2*xspacer+3*innermargin/2,margin+2*yspacer+3*innermargin/2,width-margin-innermargin,margin+2*yspacer+3*innermargin/2);
   //h1
   line(margin+2*xspacer+3*innermargin/2,margin-innermargin,width-margin-innermargin,margin-innermargin);
   //v2
   line(width-margin-innermargin,margin-innermargin,width-margin-innermargin,margin+2*yspacer+3*innermargin/2);
  
   //  noStroke();
      
     fill(255);
     ellipse(margin+2*xspacer+5*innermargin/2,margin+2*yspacer+3*innermargin/2,3,3);  
     ellipse(margin+4*xspacer+3*innermargin/2,margin+2*yspacer+3*innermargin/2,3,3);
     ellipse(margin+3*xspacer+3*innermargin/2,margin+2*yspacer+3*innermargin/2,3,3);
     
     ellipse(margin+2*xspacer+3*innermargin/2,margin+2*yspacer+innermargin/2,3,3);  
     ellipse(margin+2*xspacer+3*innermargin/2,margin+yspacer+innermargin/2,3,3);
     ellipse(margin+2*xspacer+3*innermargin/2,margin,3,3);
     
    fill(255,0,0);
    textSize(15);
     text("select a plot for display", 100,100);
     textSize(10);
    fill(255);
 }