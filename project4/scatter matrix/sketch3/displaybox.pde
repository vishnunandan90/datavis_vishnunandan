void displaybox(int a, int b){
  clear();
  background(0);
 
  text(max(lines,a)/2,margin+4*xspacer+3*innermargin/2,margin+2*yspacer+3*innermargin/2+10);
  text(max(lines,a),margin+3*xspacer+3*innermargin/2,margin+2*yspacer+3*innermargin/2+10);
    background(0);
   disp();
    text(lines.get(0).split(",")[a],xspacer+margin+2*xspacer+3*innermargin/2,margin+2*yspacer+20+3*innermargin/2+20);
    text(lines.get(0).split(",")[b],margin-40+2*xspacer+3*innermargin/2-20,yspacer+margin-innermargin);
   
int i =1;
while(i<lines.size()){
 pieces = lines.get(i).split(",");
    
    x = map(float(pieces[a]),0,max(lines,a),margin,margin+2*xspacer+innermargin);
    y = map(float(pieces[b]),0,max(lines,b),height-margin-2*yspacer-3*innermargin/2,margin);  
     fill(255);
    ellipse(x+innermargin/2+2*xspacer,y,2,2);
    i++;
}
   text("0",margin+2*xspacer+5*innermargin/2,margin+2*yspacer+3*innermargin/2+10);
     text("0",margin+2*xspacer+3*innermargin/2-10,margin+2*yspacer+innermargin/2);
   text(max(lines,a)/2,margin+3*xspacer+3*innermargin/2,margin+2*yspacer+3*innermargin/2+10);
  text(max(lines,a),margin+4*xspacer+3*innermargin/2,margin+2*yspacer+3*innermargin/2+10);
   text(max(lines,b)/2,margin+2*xspacer+3*innermargin/2-50,margin+yspacer+innermargin/2);
  text(max(lines,b),margin+2*xspacer+3*innermargin/2-50,margin);
  
}